package com.angelsit.ardemo.view

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.angelsit.ardemo.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        goToArBtn.setOnClickListener { goToArView() }
    }

    private fun goToArView() {
        val intent = Intent(this, ArActivity::class.java)
        startActivity(intent)
    }
}
