package com.angelsit.ardemo.view

import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.view.MotionEvent
import com.angelsit.ardemo.R
import com.google.ar.core.HitResult
import com.google.ar.core.Plane
import com.google.ar.sceneform.AnchorNode
import com.google.ar.sceneform.animation.ModelAnimator
import com.google.ar.sceneform.math.Vector3
import com.google.ar.sceneform.rendering.ModelRenderable
import com.google.ar.sceneform.rendering.Texture
import com.google.ar.sceneform.ux.ArFragment
import com.google.ar.sceneform.ux.TransformableNode
import kotlinx.android.synthetic.main.activity_ar.*
import java.util.concurrent.CompletableFuture

class ArActivity : AppCompatActivity() {

    private lateinit var arFragment: ArFragment

    private var printerRenderable: ModelRenderable? = null

    private var alreadyBuilt = false

    private var animator: ModelAnimator? = null

    private var currentState = 1

    private var originalSize = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ar)

        arFragment = ux_fragment as ArFragment

        ModelRenderable
            .builder()
            .setSource(this, R.raw.printer2)
            .build()
            .thenAccept { renderable ->
                printerRenderable = renderable

            }
            .exceptionally { t: Throwable ->
                print(t.message)
                return@exceptionally null
            }

        arFragment.setOnTapArPlaneListener { hitResult: HitResult, plane: Plane, motionEvent: MotionEvent ->
            if (printerRenderable == null) {
                return@setOnTapArPlaneListener
            }


            if (currentState < 2) {
                // Create the Anchor.
                val anchor = hitResult.createAnchor()
                val anchorNode = AnchorNode(anchor)
                anchorNode.setParent(arFragment.arSceneView.scene)

                /* button.setOnClickListener({ v ->
                     anchorNode.anchor!!.detach()
                     anchorNode.setParent(null)
                 })*/
                // Create the transformable andy and add it to the anchor.
                val andy = TransformableNode(arFragment.transformationSystem)
                andy.setParent(anchorNode)
                andy.renderable = printerRenderable
                andy.localScale = Vector3(0.3f, 0.3f, 0.3f)
                andy.select()
                alreadyBuilt = true
                currentState++

                val a = printerRenderable!!.getAnimationData(0)
                animator = ModelAnimator(a, printerRenderable)
            } else {
                playAnim()
            }


        }

        resetButton.setOnClickListener { onResetScene() }
        homeButton.setOnClickListener { onGoHome() }
    }

    private fun onResetScene() {
        /*if (printerRenderable != null) {

        }*/
        if (currentState > 1 && animator != null) {
            currentState = 2
            animator!!.end()
            animator!!.cancel()
        }

    }

    fun onToggleSize() {


    }

    private fun onGoHome() {
        finish()
    }

    private fun playAnim() {
        when (currentState) {
            2 -> playFirstAnim()
            3 -> playSecondAnim()
            4 -> playThirdAnim()
        }
    }

    private fun setTexture() {

        Texture.builder()
            .setSource(this, Uri.parse("https://www.xszz.org/skin/wt/rpic/t13.jpg"))
            .setUsage(Texture.Usage.DATA)
            .build()
            .thenAccept {
                val texture = it
                printerRenderable!!.getMaterial(0).setTexture("roughness", it)
                /*printerRenderable!!.getMaterial(1).setTexture("baseColor", it)
                printerRenderable!!.getMaterial(2).setTexture("baseColor", it)
                printerRenderable!!.getMaterial(3).setTexture("baseColor", it)*/
            }
            .exceptionally {
                print(it.message)
                return@exceptionally null
            }


    }

    private fun playFirstAnim() {
        if (animator != null) {
            animator!!.start()
            //setTexture()
            Handler().postDelayed({
                animator!!.pause()
                currentState++
            }, ANIM_DURATIONS[0])
        }


    }


    private fun playSecondAnim() {
        if (animator != null) {
            animator!!.resume()

            Handler().postDelayed({
                animator!!.pause()
                currentState++
            }, ANIM_DURATIONS[1])
        }
    }

    private fun playThirdAnim() {

        if (animator != null) {
            animator!!.resume()

            Handler().postDelayed({
                animator!!.end()
                currentState++
            }, ANIM_DURATIONS[2])
        }

    }


    companion object {

        val states = mapOf(
            0 to "SURFACE_NOT_FOUND",
            1 to "READY_TO_BUILD",
            2 to "BUILT",
            3 to "FIRST_ANIM_FINISH",
            4 to "SECOND_ANIM_FINISH",
            5 to "THIRD_ANIM_FINISH"
        )

        val ANIM_DURATIONS = arrayOf(2000L, 2000L, 2000L)
    }
}
